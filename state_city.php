<?php
require_once 'db_config.php';

// starting connection with database
$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS);
mysqli_select_db($conn, DB_NAME);
mysqli_query($conn, "SET NAMES " . DB_COLL);

// initializing cities variable and define a query to fetch it
$cities = null;
if (isset($_POST['state'])) {
    $state = $_POST['state'];
    $cityRes = $conn->query("SELECT * FROM city WHERE state_id=$state");
    $cities = array();
    while ($row = $cityRes->fetch_assoc()) {
        $cities [] = $row;
    }

    // sharing collected cities as final results
    $final = '';
    foreach ($cities as $city) {
        $id = $city['id'];
        $name = $city['name'];
        $final .= "<option value=$id>$name</option>";
    }
    exit($final);
}

// defining state variable and a query for fetch it
$result = $conn->query("SELECT * FROM `state`");

$states = array();
while ($row = $result->fetch_assoc()) {
    $states [] = $row;
}
