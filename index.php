<?php
    require_once 'state_city.php';
?>

<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>انتحاب شهر و استان</title>
    <style>
        body {
            display: flex;
            justify-content: space-around;
            background: #C6FFDD; /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #f7797d, #FBD786, #C6FFDD); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #f7797d, #FBD786, #C6FFDD); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        }

        .wrapper {
            direction: rtl;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="pre_paragraph">
        <p>ابتدا استان و سپس شهر مورد نظر را از بین گزینه ها انتحاب کنید</p>
    </div>
    <div class="city_selection">
        <label for="state">استان</label>
        <select name="state" id="state">
            <option selected></option>
            <?php foreach ($states as $state) { ?>
                <option value="<?php echo $state['id'] ?>"><?php echo $state['name'] ?></option>
            <?php } ?>
        </select>
        <label for="city">شهر</label>
        <select name="city" id="city">
        </select>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">

</script>
<script>
    $('#state').on('change', function (e) {
        // console.log('errors');
        e.preventDefault();
        var selectData = 'state=' + $(this).val();
//         console.log(selectData);

        $.ajax({
            type: 'post',
            url: $(location).attr('href'),
            data: selectData,
            success: function (data) {
                $('#city').html(data);

            },
            error: function (data) {
                console.log(data);
            }
        });
    });
</script>
</body>
</html>